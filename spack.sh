#!/bin/bash
set -e
# Usage: ./water.sh {GPU_COUNT} {IMG_NAME}

# Script arguments
GPU_COUNT=0

# remember to change as we do more cores.......
OMP_NUM_THREADS=24
# Set number of OpenMP threads
export OMP_NUM_THREADS=${OMP_NUM_THREADS:-1}

spack load gromacs

# Create a directory on the host to work within
mkdir -p ./work
cd ./work

# Download benchmark data
DATA_SET=water_GMX50_bare
wget -c ftp://ftp.gromacs.org/pub/benchmarks/${DATA_SET}.tar.gz
tar xf ${DATA_SET}.tar.gz


# Change to the benchmark directory
cd ./water-cut1.0_GMX50_bare/1536

# Singularity will mount the host PWD to /host_pwd in the container
#SINGULARITY="singularity run --nv -B ${PWD}:/host_pwd --pwd /host_pwd ${SIMG}"

# Prepare benchmark data
gmx_mpi grompp -f pme.mdp

# Run benchmark
gmx_mpi mdrun \
                 -ntmpi ${GPU_COUNT} \
                 -nb cpu \
                 -ntomp ${OMP_NUM_THREADS} \
                 -pin on \
                 -v \
                 -noconfout \
                 -nsteps 5000 \
                 -s topol.tpr
