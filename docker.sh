#!/bin/bash
set -e
# Usage: ./water.sh {GPU_COUNT} {IMG_NAME}

# Script arguments
#GPU_COUNT=${1:-1}
# This macbook has 0 GPUs (so guess on # thread ranks)
GPU_COUNT=0

#SIMG=${2:-"${PWD}/gromacs-gromacs2018.2-ubuntu16.04-cuda9.0-mpi3.0.0.simg"}

DOCKER_REPO_NAME=gromacs/gromacs

# this pull is from gromacs/gromacs latest 
#REPOSITORY              TAG                   IMAGE ID            CREATED             SIZE
#gromacs/gromacs         latest                4372126e8ac8        6 days ago          1.16GB

# Unfortunately, this image was built for CPUs with rtdscp instruction (i.e. not Intel Core i7)
DOCKER_IMAGE_NAME=4372126e8ac8

# My Macbook pro 2018  (MacBookPro15,1) has an intel core i7, 6-core, 2.6GHz
OMP_NUM_THREADS=6

# Set number of OpenMP threads
export OMP_NUM_THREADS=${OMP_NUM_THREADS:-1}

# Create a directory on the host to work within
mkdir -p ./work
cd ./work

# Download benchmark data
DATA_SET=water_GMX50_bare
wget -c ftp://ftp.gromacs.org/pub/benchmarks/${DATA_SET}.tar.gz
tar xf ${DATA_SET}.tar.gz

# Change to the benchmark directory
cd ./water-cut1.0_GMX50_bare/1536

# Singularity will mount the host PWD to /host_pwd in the container
#SINGULARITY="singularity run --nv -B ${PWD}:/host_pwd --pwd /host_pwd ${SIMG}"

DOCKER="docker run -i --rm --tty --mount type=bind,source=${PWD},target=/host_pwd -w=/host_pwd ${DOCKER_IMAGE_NAME}"

#$DOCKER
#exit

# Prepare benchmark data
${DOCKER} gmx grompp -f pme.mdp

# Run benchmark
${DOCKER} gmx mdrun \
                 -ntmpi ${GPU_COUNT} \
                 -nb cpu \
                 -ntomp ${OMP_NUM_THREADS} \
                 -pin on \
                 -v \
                 -noconfout \
                 -nsteps 5000 \
                 -s topol.tpr
